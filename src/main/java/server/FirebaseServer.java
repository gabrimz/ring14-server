package server;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.logging.Logger;

public class FirebaseServer {

    private static FirebaseServer sInstance = null;

    private Logger mLogger = ServerLogger.getInstance().getLogger("Firebase");

    private Boolean mIsRunning = false;

    private DatabaseReference mDatabase;

    private boolean mIsConnected = false;

    private double mGoalDistance = 0;

    /*
     * Use two different counters. mCurrentKmDonatedInitial holds the value that is in the database when we start and
     * it's set only once. When we receive new donations, we increment mCurrentKmDonatedLocal and use the sum
     * of mCurrentKmDonatedInitial and mCurrentKmDonatedLocal. This allows to handle connection losses properly.
     */
    private BigDecimal mCurrentKmDonatedInitial = BigDecimal.ZERO;
    private BigDecimal mCurrentKmDonatedLocal = BigDecimal.ZERO;

    private FirebaseServer() {
    }

    public static synchronized FirebaseServer getInstance() {
        if (sInstance == null) {
            sInstance = new FirebaseServer();
        }
        return sInstance;
    }

    public synchronized void start(String credentialPath, String databaseUrl, boolean neverDropConnection)  {
        if (mIsRunning) {
            mLogger.warning("The server is already running");
            return;
        }
        mIsRunning = true;

        FileInputStream fis;
        try {
            fis = new FileInputStream(credentialPath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        // Initialize the app with a service account, granting admin privileges
        FirebaseOptions options = new FirebaseOptions.Builder()
                .setDatabaseUrl(databaseUrl)
                .setServiceAccount(fis)
                .build();
        FirebaseApp.initializeApp(options);

        // As an admin, the app has access to read and write all data, regardless of Security Rules
        mDatabase = FirebaseDatabase.getInstance().getReference();

        // Add a listener so that the server stays connected with Firebase
        if (neverDropConnection) {
            DatabaseReference dbForListener = mDatabase.child("something_to_keep_the_connection_alive");
            dbForListener.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    // Don't do anything
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Don't do anything
                }
            });
        }
    }

    public void waitConnection() throws InterruptedException {
        synchronized (this) {
            if (!mIsRunning) {
                throw new IllegalStateException("FirebaseServer not started");
            }
        }

        mDatabase.child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    mLogger.info("Connected to Firebase database");
                    mIsConnected = true;
                } else {
                    mLogger.info("Disconnected from Firebase database");
                    mIsConnected = false;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mLogger.severe("Cancelled " + databaseError.getMessage());
            }
        });

        String totalPathBase = Constants.FIREBASE_TABLE_TOTAL_ROOT + "/" + Constants.DONATION_SESSION + "/";

        /* Get initial values as soon as we start */
        mDatabase.child(totalPathBase + Constants.FIREBASE_TABLE_CURRENT_GOAL)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        mGoalDistance = snapshot.getValue(Double.class);
                        mLogger.info("Current goal: " + mGoalDistance);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        mLogger.severe("Get current goal cancelled: "  + databaseError.getMessage());
                    }
                });

        mDatabase.child(totalPathBase + Constants.FIREBASE_TABLE_CURRENT_DISTANCE)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        mCurrentKmDonatedInitial = new BigDecimal(snapshot.getValue(Double.class));
                        mLogger.info("Current distance: " + mCurrentKmDonatedInitial);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        mLogger.severe("Get current distance cancelled: "  + databaseError.getMessage());
                    }
                });
    }

    public boolean isConnected() {
        return mIsConnected;
    }

    public DatabaseReference getDatabaseReference() {
        return mDatabase;
    }

    public synchronized void addDonationToTotal(double kmDonated) {
        mCurrentKmDonatedLocal = mCurrentKmDonatedLocal.add(BigDecimal.valueOf(kmDonated));
        mLogger.info("Adding donation, distance=" + kmDonated + ", total=" + getCurrentTotalDistance());
        updateDatabaseValue();
    }

    private double getCurrentTotalDistance() {
        return mCurrentKmDonatedInitial.add(mCurrentKmDonatedLocal).doubleValue();
    }

    private void updateDatabaseValue() {
        mDatabase.child(Constants.FIREBASE_TABLE_TOTAL_ROOT + "/" + Constants.DONATION_SESSION + "/" +
                Constants.FIREBASE_TABLE_CURRENT_DISTANCE).setValue(getCurrentTotalDistance());
    }
}
