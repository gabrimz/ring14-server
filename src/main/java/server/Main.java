package server;

import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.ValidationError;
import com.google.firebase.database.DatabaseReference;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.commons.cli.*;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.util.logging.Logger;

public class Main {

    private static BraintreeHelper sBraintreeHelper;

    public static void main(String[] args) throws InterruptedException, IOException {

        Options options = new Options();
        Option option;

        option = new Option("p", "port", true, "FirebaseServer port");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("c", "credentials", true, "Path to the Firebase credential file");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("u", "url", true, "URL of the Firebase database");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("b", "braintree-config", true, "Path to the Braintree properties file");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("l", "logfile", true, "Path to log file");
        option.setRequired(false);
        options.addOption(option);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            Logger.getAnonymousLogger().severe(e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar firebaseServer.jar OPTIONS", options);
            System.exit(1);
            return;
        }

        /* Parse this first so that we use the same config for all the loggers */
        if (cmd.hasOption("l")) {
            String logFile = cmd.getOptionValue("l");
            ServerLogger.getInstance().setLogPath(logFile, true);
            ServerLogger.getInstance().saveToFile(true);
        }

        int port = Integer.parseInt(cmd.getOptionValue("p"));
        String credentialPath = cmd.getOptionValue("c");
        String databaseUrl = cmd.getOptionValue("u");
        final FirebaseServer firebaseServer = FirebaseServer.getInstance();

        String apiKeysPath = cmd.getOptionValue("b");
        sBraintreeHelper =  BraintreeHelper.getInstance(apiKeysPath);

        firebaseServer.start(credentialPath, databaseUrl, true);
        firebaseServer.waitConnection();

        /* Start HTTP firebaseServer to remotely check the status of the connection */
        HttpServer httpServer = HttpServer.create(new InetSocketAddress(port), 0);
        httpServer.createContext(Constants.STATUS_PATH, new HttpHandlerStatus(firebaseServer));
        httpServer.createContext(Constants.GET_TOKEN_PATH, new HttpHandlerGetToken());
        httpServer.createContext(Constants.PAYMENT_NONCE_PATH, new HttpHandlerPaymentNonce(firebaseServer));
        httpServer.setExecutor(null);
        httpServer.start();
    }

    private static class HttpHandlerStatus implements HttpHandler {

        private FirebaseServer mFirebaseServer;

        private HttpHandlerStatus(FirebaseServer firebaseServer) {
            mFirebaseServer = firebaseServer;
        }

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            String response = mFirebaseServer.isConnected() ? Constants.SERVER_READY_RESPONSE_STRING :
                    Constants.SERVER_NOT_READY_RESPONSE_STRING;
            httpExchange.sendResponseHeaders(HttpStatus.SC_OK, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    private static class HttpHandlerGetToken implements HttpHandler {

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            String response = sBraintreeHelper.getClientToken();
            httpExchange.sendResponseHeaders(HttpStatus.SC_OK, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    private static class HttpHandlerPaymentNonce implements HttpHandler {

        private FirebaseServer mFirebaseServer;

        private HttpHandlerPaymentNonce(FirebaseServer firebaseServer) {
            mFirebaseServer = firebaseServer;
        }

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            int responseCode;
            String response;

            /* TODO: Create a map of pending requests to handle requests sent multiple times */

            if ("post".equalsIgnoreCase(httpExchange.getRequestMethod())) {
                InputStreamReader isr =
                        new InputStreamReader(httpExchange.getRequestBody(), "UTF-8");
                BufferedReader br = new BufferedReader(isr);
                StringBuilder buf = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    buf.append(line);
                }
                br.close();
                isr.close();

                try {
                    JSONObject queryJson = new JSONObject(buf.toString());
                    if (!queryJson.has(Constants.KEY_PAYMENT_AMOUNT) ||
                            !queryJson.has(Constants.KEY_PAYMENT_NONCE) ||
                            !queryJson.has(Constants.KEY_FIREBASE_UID)) {
                        throw new IllegalArgumentException();
                    }

                    /* Parse the JSON immediately so that we get JSONException right away */
                    String nonce = queryJson.getString(Constants.KEY_PAYMENT_NONCE);
                    BigDecimal amount = BigDecimal.valueOf(queryJson.getDouble(Constants.KEY_PAYMENT_AMOUNT));
                    double distance = amount.floatValue() * Constants.EUR_TO_KM;
                    String uid = queryJson.getString(Constants.KEY_FIREBASE_UID);

                    Result<Transaction> result = sBraintreeHelper.handlePaymentNonce(nonce, amount);

                    if (result.isSuccess()) {
                        Transaction transaction = result.getTarget();
                        responseCode = HttpStatus.SC_OK;
                        response = saveTransaction(transaction, uid, distance);
                        mFirebaseServer.addDonationToTotal(distance);
                    } else {
                        String errorString = "";
                        for (ValidationError error : result.getErrors().getAllDeepValidationErrors()) {
                            errorString += "Error: " + error.getCode() + ": " + error.getMessage() + "\n";
                        }
                        responseCode = HttpStatus.SC_INTERNAL_SERVER_ERROR;
                        response = errorString;
                    }
                } catch (JSONException | IllegalArgumentException e) {
                    e.printStackTrace();
                    responseCode = HttpStatus.SC_BAD_REQUEST;
                    response = "Invalid request";
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                    responseCode = HttpStatus.SC_INTERNAL_SERVER_ERROR;
                    response = "There was an error processing the request";
                }
            } else {
                responseCode = HttpStatus.SC_BAD_REQUEST;
                response = "POST requests only";
            }

            httpExchange.sendResponseHeaders(responseCode, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }

        private String saveTransaction(Transaction transaction, String uid, double distance) {
            String transactionId = transaction.getId();
            float amount = transaction.getAmount().floatValue();

            long timestamp = System.currentTimeMillis();

            DatabaseReference dbRefGlobal = mFirebaseServer.getDatabaseReference()
                    .child(Constants.FIREBASE_TABLE_DONATION_ROOT);
            String key = dbRefGlobal.push().getKey();

            dbRefGlobal.child(key).child(Constants.FIREBASE_TABLE_DONATION_AMOUNT).setValue(amount);
            dbRefGlobal.child(key).child(Constants.FIREBASE_TABLE_DONATION_DISTANCE).setValue(distance);
            dbRefGlobal.child(key).child(Constants.FIREBASE_TABLE_DONATION_TIMESTAMP).setValue(timestamp);
            dbRefGlobal.child(key).child(Constants.FIREBASE_TABLE_DONATION_UID).setValue(uid);
            dbRefGlobal.child(key).child(Constants.FIREBASE_TABLE_DONATION_TRANSACTION_ID).setValue(transactionId);

            DatabaseReference dbRefUser = mFirebaseServer.getDatabaseReference()
                    .child(Constants.FIREBASE_TABLE_USER_ROOT + uid + Constants.FIREBASE_TABLE_DONATION_ROOT);
            dbRefUser.child(key).child(Constants.FIREBASE_TABLE_DONATION_AMOUNT).setValue(amount);
            dbRefUser.child(key).child(Constants.FIREBASE_TABLE_DONATION_DISTANCE).setValue(distance);
            dbRefUser.child(key).child(Constants.FIREBASE_TABLE_DONATION_TIMESTAMP).setValue(timestamp);
            dbRefUser.child(key).child(Constants.FIREBASE_TABLE_DONATION_TRANSACTION_ID).setValue(transactionId);

            return key;
        }
    }
}
