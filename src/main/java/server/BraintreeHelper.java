package server;

import com.braintreegateway.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.logging.Logger;

public class BraintreeHelper {

    private static BraintreeHelper sInstance = null;

    private static final boolean PRODUCTION = false;

    private static BraintreeGateway sGateway;

    private Logger mLogger = ServerLogger.getInstance().getLogger("Braintree");

    private BraintreeHelper(String propertiesFile) throws IOException {
        Properties props = new Properties();
        props.load(new FileInputStream(propertiesFile));
        String merchantId = props.getProperty("merchantId");
        String publicKey = props.getProperty("publicKey");
        String privateKey = props.getProperty("privateKey");
        if (merchantId == null || publicKey == null || privateKey == null) {
            throw new InvalidPropertiesFormatException("Missing property");
        }
        sGateway = new BraintreeGateway(Environment.SANDBOX, merchantId, publicKey, privateKey);
    }

    public static synchronized BraintreeHelper getInstance(String apiKeysPath)
            throws IOException {
        if (sInstance == null) {
            sInstance = new BraintreeHelper(apiKeysPath);
        }
        return sInstance;
    }

    public String getClientToken() {
        return sGateway.clientToken().generate();
    }

    public Result<Transaction> handlePaymentNonce(String nonce, BigDecimal amount) {
        TransactionRequest request = new TransactionRequest()
                .amount(amount)
                .paymentMethodNonce(PRODUCTION ? nonce : "fake-valid-nonce")
                .options()
                .submitForSettlement(true)
                .done();
        Result<Transaction> result = sGateway.transaction().sale(request);
        if (result.isSuccess()) {
            mLogger.info("Nonce=" + nonce + "(" + amount + ") accepted");
        } else {
            mLogger.info("Nonce=" + nonce + "(" + amount + ") rejected");
        }
        return result;
    }
}
