package server;

import java.io.IOException;
import java.util.logging.*;

public class ServerLogger {

    private static ServerLogger sInstance;

    private boolean mSaveToFile;
    private Handler mHandler;

    public static synchronized ServerLogger getInstance() {
        if (sInstance == null) {
            sInstance = new ServerLogger();
        }
        return sInstance;
    }

    public Logger getLogger(String tag) {
        Logger logger = Logger.getLogger(tag);
        if (mSaveToFile && mHandler != null) {
            logger.addHandler(mHandler);
        }
        return logger;
    }

    public void setLogPath(String logPath, boolean append) {
        try {
            mHandler = new FileHandler(logPath, append);
            mHandler.setLevel(Level.ALL);
            mHandler.setFormatter(new SimpleFormatter());
        } catch (IOException e) {
            Logger.getAnonymousLogger().log(Level.SEVERE, "Could not open " + logPath + ", logs won't be saved");
        }
    }

    public void saveToFile(boolean save) {
        mSaveToFile = save;
    }
}
